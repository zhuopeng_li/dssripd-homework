# DSSRIPD Task 7&8 WebGL Solution

This documentation will walk you through step-by-step to achieve task 7 and 8 of the course *Digital and Scientific Skills for Interdisciplinary Research for Prototyping Project*.

## Getting the URDF and glTF Models

1. Starting with your Fusion 360 model, make sure it is as clean and neat as possible, and most important, put a small, regular **'end-effector'** as highlighted in the picture below. ![sda](./docs/1.png)

2. Download *fusion2urdf* plugin from [here](https://github.com/SpaceMaster85/fusion2urdf) and put the content of the repo to `%appdata%\Autodesk\Autodesk Fusion 360\API\Scripts\fusion2urdf`, your folder should look like this:
   
   ```bash
   c/Users/<your username>/AppData/Roaming/Autodesk/Autodesk Fusion 360/API/Scripts
   └── fusion2urdf
       ├── Example
       ├── LICENSE
       ├── README.md
       └── URDF_Exporter
   ```
   
    After that, export your model as *URDF* format, select *ROS 1* on the pop-up window
   
   <img title="" src="./docs/3.png" alt="dad" width="616"> 
   
   Then move the output to a linux environment, windows user should consider install [windows subsystem for linux](https://docs.microsoft.com/en-us/windows/wsl/install)

3. Download xacro2urdf plugin from [here](https://github.com/doctorsrn/xacro2urdf), before running the script, change all geometry paths in the xacro file from `package://<yourmodel>/meshes/<yourcomponent>.stl` to `../meshes/<yourcomponent>.gltf`, as shown in the next picture:
   
   ![4.png](./docs/4.png)
   
   After that, run the python script as instructed in the *How to use* section of the github page above and move the output file to the `urdf` folder

4. Convert your stl models under `meshes` folder to glTF file on [this website](https://products.aspose.app/3d/conversion/stl-to-gltf), and move all output files back to the `meshes` folder

5. At this point, you model/assets should look like this:
   
   ```bash
   $yourmodel/
   ├── meshes
   │   ├── arm1.gltf
   │   ├── arm2.gltf
   │   ├── base_link.gltf
   │   ├── end.gltf
   │   ├── pin.gltf
   │   └── rotator.gltf
   └── urdf
    └── fusion2urdf.urdf
   ```

## Building Node.js server

1. Install Node.js on Linux, see [official instruction](https://nodejs.org/en/download/package-manager/)

2. Clone the Node app repository from [here](https://gitlab.com/zhuopeng_li/dssripd-homework)

3. Move your model to `urdf` folder

4. Inside the repository, open `javascript/exmaple/src/index.js`, add your own joints between line 187 and line 205

5. Open `javascript/example/src/simple.js`, edit the configurations between line 35 to 44, 133 to 143, 249 to 250 and 361 to 363, see comments in the file for detail 

6. In bash, change directory to `javascript` folder and run `npm install`, after installation, don't audit, run `npm start`

7. Visit http://localhost:9080/javascript/example/simple.html to see the model

## Controlling the model by Node-RED via MQTT

1. Run following command to install Node-RED on Windows
   
   ```powershell
   npm install -g --unsafe-perm node-red
   ```

2. Open `%userprofile%\.node-red\settings.js`, enable the project feature on line 341

3. Visit your Node-RED server panel and clone the project repository from [here](https://gitlab.com/zhuopeng_li/dssripd-homework-nodered)

4. Copy the token from model website and paste it on Node-RED Dashboard
   
   ![5.png](./docs/5.png)

## Credits

This work is based on Orwa Diraneyya's project: [virtual-programming-lab](https://gitlab.com/rwth-crmasters-wise2122/urdf-loaders) and [nodered-virtual-programming-lab](https://gitlab.com/rwth-crmasters-wise2122/nodered-virtual-programming-lab)


