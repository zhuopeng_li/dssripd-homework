import paho.mqtt.client as mqtt
import json
import time
import numpy as np
import math

encoder = json.JSONEncoder()

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected.")
    exit()

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # client.subscribe("$SYS/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    return 

def triple_to_arr(str):
    return list(map(float, str.split(' ')))

def rpy_to_R(rpy):
    if isinstance(rpy, str):
        arr = triple_to_arr(rpy)
    elif isinstance(rpy, np.ndarray):
        arr = rpy.tolist()

    roll = arr[0]
    pitch = arr[1]
    yaw = arr[2]

    Rz_yaw = np.array([
        [np.cos(yaw), -np.sin(yaw), 0],
        [np.sin(yaw),  np.cos(yaw), 0],
        [          0,            0, 1]])
    Ry_pitch = np.array([
        [ np.cos(pitch), 0, np.sin(pitch)],
        [             0, 1,             0],
        [-np.sin(pitch), 0, np.cos(pitch)]])
    Rx_roll = np.array([
        [1,            0,             0],
        [0, np.cos(roll), -np.sin(roll)],
        [0, np.sin(roll),  np.cos(roll)]])
    # R = RzRyRx
    R = np.dot(Rz_yaw, np.dot(Ry_pitch, Rx_roll))
    return np.around(R, 2)

def homogenous_T(P, rpy, q):

    R = np.array(rpy_to_R(rpy))
    P = np.array(triple_to_arr(P)).reshape((3,1))
    Q = rpy_to_R(q)
    R = R.dot(Q)
    a = np.array([0, 0, 0, 1])

    # print(R)
    # print(P)
    # print(a)
    # print(np.vstack((np.hstack((R, P)), a)))
    # exit()

    return (np.vstack((np.hstack((R, P)), a)))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("broker.emqx.io", 1883, 60)

client.loop_start()

# print(rpy_to_R("1.57 0 0"))
# Kabine_zu_Radlauf2
# T0  = homogenous_T("0.68 -0.63 0.38", "0 0 1.57", np.array([0, 0, 0]))
# Rev4
T0  = homogenous_T("0.0 0.18 0.0", "0 0 0", np.array([0, 0, 0]))
# BaggerVerbindungArm_zu_Kabine
# T1 = homogenous_T("0.3 -0.31 0.08" , "0 0 1.57", np.array([0, 0, 0]))
# BaggerVerbindungArm2_zu_BaggerVerbindungArm
# T2  = homogenous_T("-0.142 0.107 0.02", "0 0 0", np.array([0, 0, 0]))
# Rev5
T1 = homogenous_T("0.333536 1.921744 -0.125", "0 0 0", np.array([0, 0, 0]))
# Arm1_zu_BaggerVerbindungArm2
# T3  = homogenous_T("-0.17 -0.08 0.09", "0 0 0", np.array([0, 0, 0]))
# Arm2_zu_Arm1
# T4  = homogenous_T("-1.4 0.02 1.06", "0 0 0", np.array([0, 0, 0]))
# T5  = homogenous_T("", "", np.array([0, 0, 0]))

# Tr1 = T0.dot(T1)
# Tr2 = Tr1.dot(T2)
# Tr3 = Tr2.dot(T3)
# Tr4 = Tr3.dot(T4)
# Tr = Tr4
# Pr = Tr[0:3, 3]

Tr1 = T0.dot(T1)
Tr = Tr1
Pr = Tr[0:0, 0]

msg = client.publish("rwth/bcma/sessions/gQqFR3YxKuvn5Q/command", \
    payload=encoder.encode({ "command": "setTargetPosition", \
        "x": Pr[0], "y": [1], "z": Pr[2] } ), qos=0, retain=False)

# print(' '.join(map(str, np.around(Tr1[0:3, 3], 6).tolist())))
# print(' '.join(map(str, np.around(Tr2[0:3, 3], 6).tolist())))
# print(' '.join(map(str, np.around(Tr3[0:3, 3], 6).tolist())))
# print(' '.join(map(str, np.around(Tr4[0:3, 3], 6).tolist())))

# print(T1)
# print(T2)

# print(' '.join(map(str, np.around(T1.dot(T2)[0:3, 3], 6).tolist())))
# print(T1.dot(T2))

print("<T0>\n", T0)
# print("<T1+T2>\n", T1.dot(T2))
# print("<T3>\n", T3)
# print("<T4>\n", T4)

# msg.wait_for_publish()

client.loop_forever(timeout=1.0, max_packets=1, retry_first_connection=False)